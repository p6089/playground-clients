package io.belldj.pg.mn;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.TestInstance.Lifecycle.*;

import io.micronaut.core.annotation.NonNull;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import io.micronaut.test.support.TestPropertyProvider;
import jakarta.inject.Inject;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.*;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.*;
import org.testcontainers.utility.DockerImageName;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedDeque;

@Testcontainers 
@MicronautTest
@TestInstance(PER_CLASS)
class HealthFT implements TestPropertyProvider {

    @Client("/health")
    interface Health {
        @Get Map<String,Object> getHealth();
    }

    @Container
    private static final KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:latest"));

    private static final Collection<String> received = new ConcurrentLinkedDeque<>();

    @Inject Flyway flyway;
    @Inject
    Health healthEndpoint;

    @AfterEach
    public void afterEach() {
        flyway.clean();
        received.clear();
    }

    @BeforeEach
    public void beforeEach() {
        flyway.migrate();
    }

    @NonNull
    @Override
    public Map<String, String> getProperties() {
        return Map.of(
          "kafka.bootstrap.servers", kafka.getBootstrapServers()
        );
    }

    @Test
    @DisplayName("GET Health")
    void testAdd() { // POST
        var health = this.healthEndpoint.getHealth();
        assertThat(health).hasSize(3).containsEntry("status", "UP");
    }

}